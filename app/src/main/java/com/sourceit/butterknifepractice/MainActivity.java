package com.sourceit.butterknifepractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.label)
    TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void onClick() {
        Toast.makeText(this, "click detected", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button)
    public void onClickSecond() {
        label.setText("cool!");
        Toast.makeText(this, "click detected", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.action_save)
    public void onClickThird() {
        Toast.makeText(this, "click detected", Toast.LENGTH_SHORT).show();
    }

    @OnTextChanged(R.id.input)
    public void onTextChange(Editable s) {
        Toast.makeText(this, s.toString(), Toast.LENGTH_SHORT).show();
    }

}
